<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Dispute
 * @package App
 */
class Dispute extends Model
{
	/**
	 * @var array
	 */
	protected $fillable = ['created_by','category_id','description','is_private','title','is_closed'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function mediators()
	{
		return $this->belongsToMany(User::class, 'shared_disputes', 'dispute_id', 'mediator_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function creator()
	{
		return $this->belongsTo(User::class, 'created_by');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function offers()
	{
		return $this->hasMany(Offer::class);
	}
}
