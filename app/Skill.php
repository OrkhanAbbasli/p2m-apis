<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Skill
 * @package App
 */
class Skill extends Model
{
	/**
	 * @var array
	 */
	protected $fillable = ['name'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function mediators()
	{
		return $this->belongsToMany(User::class, 'skill_mediator', 'skill_id', 'mediator_id');
	}
}
