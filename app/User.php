<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * Class User
 * @package App
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

	/**
	 * @var array
	 */
	protected $appends = [
		'rating_count',
		'rating'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function roles()
	{
		return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function contacts()
	{
		return $this->hasMany(Contact::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function categories()
	{
		return $this->belongsToMany(Category::class, 'category_mediator', 'mediator_id', 'category_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function skills()
	{
		return $this->belongsToMany(Skill::class,'skill_mediator', 'mediator_id', 'skill_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function assignedDisputes()
	{
		return $this->belongsToMany(Dispute::class, 'shared_disputes', 'mediator_id', 'dispute_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function disputes()
	{
		return $this->hasMany(Dispute::class, 'created_by');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function ratings()
	{
		return $this->hasMany(Rating::class, 'mediator_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function offers()
	{
		return $this->hasMany(Offer::class,'mediator_id','id');
	}

	/**
	 * @return mixed
	 */
	public function getRatingCountAttribute()
	{
		return $this->ratings->count();
	}

	/**
	 * @return float|int
	 */
	public function getRatingAttribute()
	{
		return $this->getRatingCountAttribute() == 0 ? 5 : $this->ratings->sum('point')/$this->getRatingCountAttribute();
	}
}
