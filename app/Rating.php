<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
	/**
	 * @var array
	 */
	protected $fillable = ['mediator_id','user_id','point','total_average_point'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function mediator()
	{
		return $this->belongsTo(User::class, 'mediator_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
