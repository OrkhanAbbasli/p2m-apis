<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Dispute
 * @package App
 */
class Offer extends Model
{
	/**
	 * @var array
	 */
	protected $fillable = ['dispute_id', 'mediator_id', 'description','price','document'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function mediator()
	{
		return $this->belongsTo(User::class,'mediator_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function dispute()
	{
		return $this->belongsTo(Dispute::class,'dispute_id');
	}

}
