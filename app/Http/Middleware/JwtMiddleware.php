<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;


class JwtMiddleware
{
	public function handle($request, Closure $next, $guard = null)
	{
		$token = $request->header('jwt-token');

		if (!$token) return response()->json(['error' => 'Sistemə daxil olmalısınız'], 401);

		try {
			$credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
		} catch (ExpiredException $e) {
			return response()->json(['error' => 'Token vaxtı bitmişdir.'], 401);
		} catch (Exception $e) {
			return response()->json(['error' => 'Tokenin tanınması zamanı xəta baş verdi'], 400);
		}

		$user = User::find($credentials->sub);

		$request->auth = $user;

		return $next($request);
	}
}