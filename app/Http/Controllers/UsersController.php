<?php

namespace App\Http\Controllers;

use App\Rating;
use App\Role;
use App\User;
use Exception;
use Illuminate\Http\Request;

class UsersController extends Controller
{
	private $request;

	/**
	 * Create a new controller instance.
	 *
	 * @param Request $request
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function index()
	{
		return response()->json(User::all(), 200);
	}

	public function topMediators()
	{
		$roles = Role::whereIn('label', [USER_ROLE_MEDIATOR_USER, USER_ROLE_MEDIATOR_COMPANY])->pluck('id')->toArray();

		$list = User::with(['roles'])->whereHas('roles', function ($query) use ($roles) {
			$query->whereIn('role_id', $roles);
		})->limit(10)->orderBy('created_at', 'desc')->get();

		return response()->json($list, 200);
	}

	public function mediators()
	{
		$roles = Role::whereIn('label', [USER_ROLE_MEDIATOR_USER, USER_ROLE_MEDIATOR_COMPANY])->pluck('id')->toArray();

		$select = User::select();

		$select->with(['roles'])->whereHas('roles', function ($query) use ($roles) {
			$query->whereIn('role_id', $roles);
		});

		if ($this->request->has('q') && !is_null($this->request->get('q')) && $this->request->get('q') != '') {
			$select->where('name', 'LIKE', '%' . $this->request->q . '%');
		}

		if ($this->request->has('categories') && !is_null($this->request->get('categories')) && $this->request->get('categories') != '') {
			$categories = (array)$this->request->categories;

			$select->with(['categories'])->whereHas('categories', function ($query) use ($categories) {
				$query->whereIn('category_id', $categories);
			});
		}

		if ($this->request->has('skills') && !is_null($this->request->get('skills')) && $this->request->get('skills') != '') {
			$skills = (array)$this->request->skills;

			$select->with(['skills'])->whereHas('skills', function ($query) use ($skills) {
				$query->whereIn('skill_id', $skills);
			});
		}

		$mediators = $select->orderBy('created_at', 'desc')->paginate(21);

		return response()->json($mediators, 200);
	}

	public function rate()
	{
		$this->validate($this->request, [
			'mediator_id' => 'required',
			'point' => 'required'
		]);

		try {
			$rating = Rating::firstOrNew([
				'user_id' => $this->request->auth->id,
				'mediator_id' => $this->request->mediator_id
			]);

			$rating->point = $this->request->point;

			$rating->save();

			return response()->json([
				'error' => null,
				'message' => 'Successfully rated',
				'data' => $rating
			], 201);

		} catch (Exception $e) {
		    return response()->json($e->getMessage(), 500);
		}
	}
}
