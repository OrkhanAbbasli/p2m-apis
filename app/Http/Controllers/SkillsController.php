<?php

namespace App\Http\Controllers;

use App\Skill;
use Illuminate\Http\Request;

class SkillsController extends Controller
{
	private $request;

	/**
	 * Create a new controller instance.
	 *
	 * @param Request $request
	 */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function skills()
	{
		return response()->json(Skill::all(), 200);
	}
}
