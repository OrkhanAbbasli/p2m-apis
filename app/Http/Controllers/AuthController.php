<?php

namespace App\Http\Controllers;

use App\Role;
use Exception;
use Illuminate\Validation\Validator;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends BaseController
{
	/**
	 * The request instance.
	 *
	 * @var \Illuminate\Http\Request
	 */
	private $request;

	/**
	 * Create a new controller instance.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return void
	 */
	public function __construct(Request $request) {
		$this->request = $request;
	}

	/**
	 * Create a new token.
	 *
	 * @param  \App\User   $user
	 * @return string
	 */
	protected function jwt(User $user) {
		$payload = [
			'iss' => "p2m-platform",
			'sub' => $user->id,
			'iat' => time(),
			'exp' => time() + 60*60,
			'user' => [
				'name' => $user->name,
				'email' => $user->email,
				'description' => $user->description,
				'role' => $user->roles->first()
			]
		];

		return JWT::encode($payload, env('JWT_SECRET'));
	}

	/**
	 * @param User $user
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function authenticate(User $user)
	{
		$this->validate($this->request, [
			'email'     => 'required|email',
			'password'  => 'required'
		]);

		$user = $user->where('email', $this->request->input('email'))->first();

		if (!$user) {
			return response()->json([
				'error' => 'Email does not exist.'
			], 400);
		}

		if (Hash::check($this->request->input('password'), $user->password)) {
			return response()->json([
				'error' => null,
				'token' => $this->jwt($user)
			], 200);
		}

		return response()->json([
			'error' => 'Email or password is wrong.'
		], 400);
	}

	public function register()
	{
		 $this->validate($this->request, [
			'name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required',
			'confirm_password' => 'required|same:password',
			'type' => 'required|in:user,mediator_user,mediator_company'
		]);

		try {
			$user = new User();

			$user->name = $this->request->name;
			$user->email = $this->request->email;
			$user->password = Hash::make($this->request->password);
			$user->save();

			$role = Role::where('label', $this->request->type)->first();

			$user->roles()->attach($role->id);

			return response()->json([
				'error' => null,
				'message' => 'User created successfully'
			],201);
		} catch (Exception $e) {
		    return response()->json($e->getMessage(), 500);
		}
	}

}