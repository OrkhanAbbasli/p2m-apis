<?php

namespace App\Http\Controllers;

use App\Offer;
use Exception;
use Illuminate\Http\Request;

class OffersController extends Controller
{
	private $request;

	/**
	 * Create a new controller instance.
	 *
	 * @param Request $request
	 */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function sendOffer($disputeId)
	{
		$this->validate($this->request, [
			//'description' => 'required',
			'price' => 'required'
		]);

		try {
			$offer = new Offer();

			$offer->dispute_id = $disputeId;
			$offer->mediator_id = $this->request->auth->id;
			$offer->price = $this->request->price;

			$offer->save();

			return response()->json([
				'error' => null,
				'message' => 'Offer created successfully',
				'data' => $offer
			]);

		} catch (Exception $e) {
		    return response()->json($e->getMessage(), 500);
		}
	}

	public function offersOfMediator()
	{
		return response()->json($this->request->auth->offers()->with('dispute')->paginate(25), 200);
	}
}
