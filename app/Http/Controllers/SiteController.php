<?php

namespace App\Http\Controllers;

use App\Dispute;
use App\Role;
use App\User;
use Laravel\Lumen\Http\Request;

class SiteController extends Controller
{
	private $request;

	/**
	 * Create a new controller instance.
	 *
	 * @param Request $request
	 */
    public function __construct(Request $request)
    {
		$this->request = $request;
    }

    public function statistics()
	{
		$roles = Role::whereIn('label', [USER_ROLE_MEDIATOR_USER, USER_ROLE_MEDIATOR_COMPANY])->pluck('id')->toArray();

		$mediators = User::whereHas('roles', function ($query) use ($roles) {
			$query->whereIn('role_id', $roles);
		})->count();

		$openDisputes = Dispute::where('is_closed', false)->count();
		$closedDisputes = Dispute::where('is_closed', true)->count();

		return response()->json([
			'mediators' => $mediators,
			'openDisputes' => $openDisputes,
			'closedDisputes' => $closedDisputes
		], 200);
	}
}
