<?php

namespace App\Http\Controllers;

use App\Category;

class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

	public function categories()
	{
		$categories = Category::all(['id','name']);

		return response()->json($categories, 200);
    }
}
