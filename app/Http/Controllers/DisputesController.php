<?php

namespace App\Http\Controllers;


use App\Dispute;
use Exception;
use Illuminate\Http\Request;

class DisputesController extends Controller
{
	/**
	 * @var Request
	 */
	private $request;

	/**
	 * Create a new controller instance.
	 *
	 * @param Request $request
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function disputes()
	{
		if ($this->request->auth->roles->first()->label == USER_ROLE_USER) {
			return $this->disputesOfUser();
		}

		return $this->disputesOfMediator();
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function publicDisputes()
	{
		$select = Dispute::select();

		$select->with(['category', 'creator'])->where('is_private', false);

		if ($this->request->has('q') && !is_null($this->request->get('q')) && $this->request->get('q') != '') {
			$select->where('title', 'LIKE', '%' . $this->request->q . '%');
		}

		if ($this->request->has('categories') && !is_null($this->request->get('categories')) && $this->request->get('categories') != '') {
			$categories = (array)$this->request->categories;

			$select->whereHas('category', function ($query) use ($categories) {
				$query->whereIn('category_id', $categories);
			});
		}

		$list = $select->paginate(25);

		return response()->json($list, 200);
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function store()
	{
		$this->validate($this->request, [
			'category' => 'required',
			'description' => 'required',
			'is_private' => 'required',
			'title' => 'required'
		]);

		if ($this->request->is_private) {
			$this->validate($this->request, [
				'mediators' => 'required'
			]);
		}

		try {
			$dispute = new Dispute();

			$dispute->created_by = $this->request->auth->id;
			$dispute->category_id = $this->request->category;
			$dispute->description = $this->request->description;
			$dispute->is_private = $this->request->is_private;
			$dispute->title = $this->request->title;

			$dispute->save();

			if ($this->request->is_private) {
				$dispute->mediators()->attach($this->request->mediators);
			}

			return response()->json([
				'error' => null,
				'message' => 'Dispute created successfully',
				'data' => $dispute
			], 201);

		} catch (Exception $e) {
			return response()->json($e->getMessage(), 500);
		}
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	private function disputesOfUser()
	{
		$select = $this->request->auth->disputes();

		$select->with(['mediators', 'category', 'offers']);

		if ($this->request->has('status')) {

			$select->where('is_closed', $this->request->get('status') == 0 ? false : true);
		}

		$disputes = $select->paginate(25);

		return response()->json($disputes, 200);
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	private function disputesOfMediator()
	{
		$select = $this->request->auth->assignedDisputes();

		if ($this->request->has('status')) {
			$select->where('is_closed', $this->request->get('status') == 0 ? false : true);
		}

		$disputes = $select->with('category')->paginate(25);

		return response()->json($disputes, 200);
	}

	public function statistics()
	{
		if ($this->request->auth->roles()->first()->label == USER_ROLE_USER){
			$disputes = $this->request->auth->disputes();
			$offers = $disputes->has('offers')->count();
		}else {
			$disputes = $this->request->auth->assignedDisputes();
			$offers = $this->request->auth->offers()->count();
		}

		$response = [
			'openDisputes' => $disputes->where('is_closed', false)->count(),
			'closedDisputes' => $disputes->where('is_closed', true)->count(),
			'offers' => $offers
		];

		return response()->json($response, 200);
	}

	public function update($id)
	{
		$this->validate($this->request, [
			'category' => 'required',
			'description' => 'required',
			'is_private' => 'required',
			'title' => 'required'
		]);

		if ($this->request->is_private) {
			$this->validate($this->request, [
				'mediators' => 'required'
			]);
		}

		try {
			$dispute = Dispute::findOrFail($id);

			$dispute->title = $this->request->title;
			$dispute->description = $this->request->description;
			$dispute->category_id = $this->request->category;
			$dispute->is_private = $this->request->is_private;
			$dispute->is_closed = $this->request->has('is_closed') ? $this->request->is_closed : false;

			$dispute->update();

			if ($this->request->is_private) {
				$mediators = $this->request->mediators;
			}else{
				$mediators = [];
			}

			$dispute->mediators()->sync($mediators);

		} catch (Exception $e) {
		    return response()->json($e->getMessage(), 500);
		}
	}
}
