<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactType extends Model
{
	/**
	 * @var array
	 */
	protected $fillable = ['label','name','pattern'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function contacts()
	{
		return $this->hasMany(Contact::class);
	}
}
