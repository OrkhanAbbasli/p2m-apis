<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	/**
	 * @var array
	 */
	protected $fillable = ['name'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function mediators()
	{
		return $this->belongsToMany(User::class, 'category_mediator', 'category_id', 'mediator_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function disputes()
	{
		return $this->hasMany(Dispute::class);
	}
}
