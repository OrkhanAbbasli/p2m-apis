<?php

$router->get('/', function () use ($router) {
	return 'P2M Platform APIS | Application Status: On';
});

$router->group(['prefix'=>'api/v1'], function () use ($router) {

	/*
	 * Statistics
	 */
	$router->get('statistics', 'SiteController@statistics');

	/*
	 * Top 10 mediators
	 */
	$router->get('mediators/top', 'UsersController@topMediators');

	/*
	 * Mediators
	 */
	$router->get('mediators', 'UsersController@mediators');

	/*
	 * Categories
	 */
	$router->get('categories', 'CategoriesController@categories');

	/*
	 * Skills
	 */
	$router->get('skills', 'SkillsController@skills');

	/*
	 * Login
	 */
	$router->post('auth/login', 'AuthController@authenticate');

	/*
	 * Registration
	 */
	$router->post('auth/register', 'AuthController@register');

	/*
	 * Routes for only authenticated users
	 */
	$router->group(['middleware' => 'jwt.auth'], function() use ($router) {
		/*
		 * Registered users
		 */
		$router->get('users', 'UsersController@index');

		/*
		 * Dispute routes
		 */
		$router->get('disputes', 'DisputesController@disputes');
		$router->get('disputes/public', 'DisputesController@publicDisputes');
		$router->post('disputes/store', 'DisputesController@store');
		$router->post('disputes/{disputeId}/offers/store', 'OffersController@sendOffer');
		$router->put('disputes/{id}/update', 'DisputesController@update');

		/*
		 * Offers of mediator
		 */
		$router->get('offers/mediator', 'OffersController@offersOfMediator');

		/*
		 * Dashboard statistics
		 */
		$router->get('statistics/dashboard', 'DisputesController@statistics');

		/*
		 * Rate any mediator
		 */
		$router->post('rate', 'UsersController@rate');
	});
});




