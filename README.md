# P2M Platform apis

For running application please run this command in your terminal
```sh
$ php -S localhost:8000 -t public
``` 

For database migrations with seeders please run this command in your terminal
```sh
$ php artisan migrate --seed
``` 
   

## Login api

  - **URI:** /api/v1/auth/login
  - **Method:** POST
  - **Request body:**
    1.  email
    2.  password
  - **Response:**
     > {
        "error": null,
        "token": "jwt-token string"
    }

## Sign up api

  - **URI:** /api/v1/auth/register
  - **Method:** POST
  - **Request body:**
    1. name: Orkhan Abbasli
    2. email: abbasli.orxan@gmail.com
    3. password: 123456
    4. confirm_password: 123456
    5. type: user
  - **Response:**
     > {
            "error": null,
            "message": "User created successfully"
        }

## Skills list

  - **URI:** /api/v1/skills
  - **Method:** GET
  - **Response:**
     > {
            "id": 1,
            "name": "Test"
        }

## Categories list

  - **URI:** /api/v1/categories
  - **Method:** GET
  - **Response:**
     > {
            "id": 1,
            "name": "Leasing"
        }

## Mediators list

  - **URI:** /api/v1/mediators
  - **Method:** GET
  - **Pagination:** page (this is query string parameter)
  - **Search:** q (this is query string parameter)
  - **Response:**
     > {
    "current_page": 1,
    "data": [
        {
            "id": 257,
            "name": "Orkhan Abbasli",
            "email": "o.abbasli@p2m.dev",
            "description": null,
            "avatar": null,
            "created_at": "2018-11-25 11:18:27",
            "updated_at": "2018-11-25 11:18:27",
            "rating_count": 0,
            "rating": 5,
            "roles": [
                {
                    "id": 3,
                    "label": "mediator_user",
                    "name": "Vasitəçi",
                    "created_at": null,
                    "updated_at": null,
                    "pivot": {
                        "user_id": 257,
                        "role_id": 3
                    }
                }
            ],
            "ratings": []
        },
    ],
    "first_page_url": "http://10.10.10.63:8000/api/v1/mediators?page=1",
    "from": 1,
    "last_page": 12,
    "last_page_url": "http://10.10.10.63:8000/api/v1/mediators?page=12",
    "next_page_url": "http://10.10.10.63:8000/api/v1/mediators?page=2",
    "path": "http://10.10.10.63:8000/api/v1/mediators",
    "per_page": 21,
    "prev_page_url": null,
    "to": 21,
    "total": 249
}

## Top 10 Mediators list

  - **URI:** /api/v1/mediators/top
  - **Method:** GET
  - **Response:**
     > [
    {
        "id": 257,
        "name": "Orkhan Abbasli",
        "email": "o.abbasli@p2m.dev",
        "description": null,
        "avatar": null,
        "created_at": "2018-11-25 11:18:27",
        "updated_at": "2018-11-25 11:18:27",
        "rating_count": 0,
        "rating": 5,
        "roles": [
            {
                "id": 3,
                "label": "mediator_user",
                "name": "Vasitəçi",
                "created_at": null,
                "updated_at": null,
                "pivot": {
                    "user_id": 257,
                    "role_id": 3
                }
            }
        ],
        "ratings": []
    },
    {
        "id": 249,
        "name": "murad.s@code.edu.az6",
        "email": "murad.s@code.edu.az472",
        "description": null,
        "avatar": null,
        "created_at": "2018-11-24 23:57:56",
        "updated_at": "2018-11-24 23:57:56",
        "rating_count": 1,
        "rating": 4,
        "roles": [
            {
                "id": 3,
                "label": "mediator_user",
                "name": "Vasitəçi",
                "created_at": null,
                "updated_at": null,
                "pivot": {
                    "user_id": 249,
                    "role_id": 3
                }
            }
        ],
        "ratings": [
            {
                "id": 1,
                "mediator_id": 249,
                "user_id": 250,
                "point": 4,
                "total_average_point": 4,
                "created_at": null,
                "updated_at": null
            }
        ]
    },
    {
        "id": 247,
        "name": "murad.s@code.edu.az6",
        "email": "murad.s@code.edu.az466",
        "description": null,
        "avatar": null,
        "created_at": "2018-11-24 23:57:55",
        "updated_at": "2018-11-24 23:57:55",
        "rating_count": 0,
        "rating": 5,
        "roles": [
            {
                "id": 3,
                "label": "mediator_user",
                "name": "Vasitəçi",
                "created_at": null,
                "updated_at": null,
                "pivot": {
                    "user_id": 247,
                    "role_id": 3
                }
            }
        ],
        "ratings": []
    },
    {
        "id": 248,
        "name": "murad.s@code.edu.az6",
        "email": "murad.s@code.edu.az471",
        "description": null,
        "avatar": null,
        "created_at": "2018-11-24 23:57:55",
        "updated_at": "2018-11-24 23:57:55",
        "rating_count": 0,
        "rating": 5,
        "roles": [
            {
                "id": 3,
                "label": "mediator_user",
                "name": "Vasitəçi",
                "created_at": null,
                "updated_at": null,
                "pivot": {
                    "user_id": 248,
                    "role_id": 3
                }
            }
        ],
        "ratings": []
    },
    {
        "id": 245,
        "name": "murad.s@code.edu.az6",
        "email": "murad.s@code.edu.az461",
        "description": null,
        "avatar": null,
        "created_at": "2018-11-24 23:57:54",
        "updated_at": "2018-11-24 23:57:54",
        "rating_count": 0,
        "rating": 5,
        "roles": [
            {
                "id": 3,
                "label": "mediator_user",
                "name": "Vasitəçi",
                "created_at": null,
                "updated_at": null,
                "pivot": {
                    "user_id": 245,
                    "role_id": 3
                }
            }
        ],
        "ratings": []
    },
    {
        "id": 246,
        "name": "murad.s@code.edu.az6",
        "email": "murad.s@code.edu.az468",
        "description": null,
        "avatar": null,
        "created_at": "2018-11-24 23:57:54",
        "updated_at": "2018-11-24 23:57:54",
        "rating_count": 0,
        "rating": 5,
        "roles": [
            {
                "id": 3,
                "label": "mediator_user",
                "name": "Vasitəçi",
                "created_at": null,
                "updated_at": null,
                "pivot": {
                    "user_id": 246,
                    "role_id": 3
                }
            }
        ],
        "ratings": []
    },
    {
        "id": 244,
        "name": "murad.s@code.edu.az6",
        "email": "murad.s@code.edu.az457",
        "description": null,
        "avatar": null,
        "created_at": "2018-11-24 23:57:53",
        "updated_at": "2018-11-24 23:57:53",
        "rating_count": 0,
        "rating": 5,
        "roles": [
            {
                "id": 3,
                "label": "mediator_user",
                "name": "Vasitəçi",
                "created_at": null,
                "updated_at": null,
                "pivot": {
                    "user_id": 244,
                    "role_id": 3
                }
            }
        ],
        "ratings": []
    },
    {
        "id": 242,
        "name": "murad.s@code.edu.az6",
        "email": "murad.s@code.edu.az455",
        "description": null,
        "avatar": null,
        "created_at": "2018-11-24 23:57:52",
        "updated_at": "2018-11-24 23:57:52",
        "rating_count": 0,
        "rating": 5,
        "roles": [
            {
                "id": 3,
                "label": "mediator_user",
                "name": "Vasitəçi",
                "created_at": null,
                "updated_at": null,
                "pivot": {
                    "user_id": 242,
                    "role_id": 3
                }
            }
        ],
        "ratings": []
    },
    {
        "id": 243,
        "name": "murad.s@code.edu.az6",
        "email": "murad.s@code.edu.az456",
        "description": null,
        "avatar": null,
        "created_at": "2018-11-24 23:57:52",
        "updated_at": "2018-11-24 23:57:52",
        "rating_count": 0,
        "rating": 5,
        "roles": [
            {
                "id": 3,
                "label": "mediator_user",
                "name": "Vasitəçi",
                "created_at": null,
                "updated_at": null,
                "pivot": {
                    "user_id": 243,
                    "role_id": 3
                }
            }
        ],
        "ratings": []
    },
    {
        "id": 240,
        "name": "murad.s@code.edu.az6",
        "email": "murad.s@code.edu.az453",
        "description": null,
        "avatar": null,
        "created_at": "2018-11-24 23:57:51",
        "updated_at": "2018-11-24 23:57:51",
        "rating_count": 0,
        "rating": 5,
        "roles": [
            {
                "id": 3,
                "label": "mediator_user",
                "name": "Vasitəçi",
                "created_at": null,
                "updated_at": null,
                "pivot": {
                    "user_id": 240,
                    "role_id": 3
                }
            }
        ],
        "ratings": []
    }
]

## Home page statistics

  - **URI:** /api/v1/statistics
  - **Method:** GET
  - **Response:**
     > {
    "mediators": 249, 
    "openDisputes": 12, 
    "closedDisputes": 0
}

## Disputes list

  - **URI:** /api/v1/disputes
  - **Method:** GET
  - **Request headers:**
    1. jwt-token

## Public disputes list

  - **URI:** /api/v1/disputes/public
  - **Method:** GET
  - **Request headers:**
    1. jwt-token

## Create dispute

  - **URI:** /api/v1/disputes/store
  - **Method:** POST
  - **Request headers:**
    1. jwt-token
  - **Request body:**
    1.category
    2.description
    3.is_private
    4.mediators (this field only required when is_private is true)
    5.title

## Sending offer for dispute

  - **URI:** /api/v1/disputes/{disputeId}/offers/store
  - **Method:** POST
  - **Request headers:**
    1. jwt-token
  - **Request body:**
    1. price

## Update dispute

  - **URI:** /api/v1/disputes/{disputeId}/offers/store
  - **Method:** PUT
  - **Request headers:**
    1. jwt-token
  - **Request body:**
    1. category
    2. description
    3. is_private
    4. title
    5. mediators (bu field yalniz is_private true oldugu halda required olur)
    6. is_closed (required deyil, disput baglandiqda true olaraq gondermek lazimdir)

## Mediator's offers list

  - **URI:** /api/v1/offers/mediator
  - **Method:** GET
  - **Request headers:**
    1. jwt-token

## Mediator's or User's dashboard statistics 

  - **URI:** /api/v1/statistics/dashboard
  - **Method:** GET
  - **Request headers:**
    1. jwt-token


## Rate api 

  - **URI:** /api/v1/rate
  - **Method:** POST
  - **Request headers:**
    1. jwt-token
  - **Request body:**
    1. mediator_id
    2. point