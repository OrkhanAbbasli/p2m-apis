<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryMediatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_mediator', function (Blueprint $table) {
			/* Categories */
			$table->integer('category_id')->unsigned()->nullable();
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
			/* Mediators */
			$table->integer('mediator_id')->unsigned()->nullable();
			$table->foreign('mediator_id')->references('id')->on('users')->onDelete('cascade');
			/* Created at & Updated at */
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_mediator');
    }
}
