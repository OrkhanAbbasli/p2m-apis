<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillMediatorPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_mediator', function (Blueprint $table) {
			/* Categories */
			$table->integer('skill_id')->unsigned()->nullable();
			$table->foreign('skill_id')->references('id')->on('skills')->onDelete('cascade');
			/* Mediators */
			$table->integer('mediator_id')->unsigned()->nullable();
			$table->foreign('mediator_id')->references('id')->on('users')->onDelete('cascade');
			/* Created at & Updated at */
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skill_mediator');
    }
}
