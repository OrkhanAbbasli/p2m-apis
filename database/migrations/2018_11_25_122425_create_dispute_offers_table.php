<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisputeOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dispute_id')->unsigned();
            $table->integer('mediator_id')->unsigned();
            $table->double('price');
            $table->mediumText('description')->nullable();
            $table->string('document')->nullable();
            $table->timestamps();

            $table->foreign('dispute_id')->references('id')->on('disputes');
            $table->foreign('mediator_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
