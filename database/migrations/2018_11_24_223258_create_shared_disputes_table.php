<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedDisputesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_disputes', function (Blueprint $table) {
            /* Cases */
			$table->integer('dispute_id')->unsigned()->nullable();
			$table->foreign('dispute_id')->references('id')->on('disputes')->onDelete('cascade');
			/* Mediators */
			$table->integer('mediator_id')->unsigned()->nullable();
			$table->foreign('mediator_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_cases');
    }
}
