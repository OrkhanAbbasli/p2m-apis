<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$roleAdmin = Role::where('label', USER_ROLE_ADMIN)->first();

		$admin = new User();
		$admin->name = 'Admin';
		$admin->email = 'admin@p2m.dev';
		$admin->password = Hash::make('roadrunners');
		$admin->description = 'System admin';
		$admin->save();

		$admin->roles()->attach($roleAdmin->id);
    }
}
