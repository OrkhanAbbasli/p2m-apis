<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('categories')->insert([
			[
				'name'  => 'Transactions'
			],
			[
				'name'  => 'Debt Recovery'
			],
			[
				'name'  => 'Insolvency'
			],
			[
				'name'  => 'Insurance'
			],
			[
				'name'  => 'Investment Funds'
			],
			[
				'name'  => 'Joint Ventures'
			],
			[
				'name'  => 'Leasing'
			],
			[
				'name'  => 'Mortgages'
			],
			[
				'name'  => 'Investments'
			],
			[
				'name'  => 'Retail Banking'
			],
			[
				'name'  => 'Trusts, Wills & Probate'
			],
			[
				'name'  => 'FX Trading'
			],
			[
				'name'  => 'Other'
			],
		]);
    }
}
