<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('roles')->insert([
			[
				'name'  => 'Admin',
				'label' => 'admin'
			],
			[
				'name'  => 'İstifadəçi',
				'label' => 'user'
			],
			[
				'name'  => 'Vasitəçi',
				'label' => 'mediator_user'
			],
			[
				'name'  => 'Vasitəçi şirkət',
				'label' => 'mediator_company'
			]
		]);
    }
}
